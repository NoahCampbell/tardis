using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TARDISRotation : MonoBehaviour
{
    GameObject crank;
    Quaternion targetRotation;
    float status;

    // Start is called before the first frame update
    void Start()
    {
        crank = transform.Find("TARDIS Rotation Crank").gameObject;
        targetRotation = Quaternion.identity;
        status = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RotateCrank()
    {
        crank.transform.Rotate(0f, 90f, 0f);
        status++;

        switch (status)
        {
            case 1:
                targetRotation = new Quaternion(0f, 0.7071068f, 0f, 0.7071068f);
                break;
            case 2:
                targetRotation = new Quaternion(0f, 1f, 0f, 0f);
                break;
            case 3:
                targetRotation = new Quaternion(0f, -0.7071068f, 0f, 0.7071068f);
                break;
            default:
                targetRotation = Quaternion.identity;
                status = 0;
                break;
        }
    }

    public Quaternion GetTargetRotation()
    {
        return targetRotation;
    }
}
