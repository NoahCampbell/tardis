using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 move;
    float horizontalRotation;
    float verticalRotation;
    PlayerMaster player;
    Quaternion cameraRotation;
    public GameObject forwards;
    TARDISMaster tardis;
    Rigidbody rb;
    Transform playerObject;
    GameObject[] playerParts;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerMaster>();
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
        rb = GetComponent<Collider>().attachedRigidbody;
        playerObject = transform.GetChild(0);
        playerParts = new GameObject[playerObject.childCount];
        
        for (int i = 0; i < playerParts.Length; i++)
        {
            playerParts[i] = playerObject.GetChild(0).gameObject;
            playerParts[i].AddComponent<PlayerPartsController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        ApplyGravity();
        Move();
        Rotate();
        SpecialButtons();
    }

    void Move()
    {
        move = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical");
        move = move.normalized * player.speed * Time.deltaTime;
        transform.Translate(move, Space.World);
        rb.velocity = Vector3.zero;
    }

    void Rotate()
    {
        horizontalRotation = Input.GetAxis("Horizontal Rotation");
        verticalRotation = -Input.GetAxis("Vertical Rotation");

        transform.Rotate(0f, horizontalRotation, 0f);
        Camera.main.transform.Rotate(verticalRotation, 0f, 0f);
    }

    public void PassingThroughTARDIS()
    {
        foreach (GameObject part in playerParts)
        {
            if (!part.GetComponent<PlayerPartsController>().GetTARDISSatus())
            {
                return;
            }
        }

        foreach (GameObject part in playerParts)
        {
            part.GetComponent<PlayerPartsController>().InvertMovement(false);
            part.transform.SetParent(null);
        }

        transform.position = playerParts[playerParts.Length/2].transform.position;
    }

    void SpecialButtons()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            bool isHit = Physics.Raycast(forwards.transform.position, forwards.transform.forward, out hit, 20f);

            if (isHit)
            {
                if (hit.transform.name.Contains("Exterior") || hit.transform.name.Contains("Door"))
                {
                    tardis.OpenExteriorDoors();
                }
                else if (hit.transform.name.Contains("Incrementor"))
                {
                    tardis.Increment();
                }
                else if (hit.transform.name.Contains("Lever"))
                {
                    tardis.Teleport();
                }
                else if (hit.transform.name.Contains("Button"))
                {
                    tardis.IncrementCoordinate(hit.transform.gameObject.name);
                }
                else if (hit.transform.name.Contains("Status"))
                {
                    tardis.SwitchButtonStatus();
                }
                else if (hit.transform.name.Contains("Rotation"))
                {
                    tardis.RotateCrank();
                }
                else
                {
                    Debug.Log(hit.transform.gameObject.name);
                }
            }
        }

        else if (Input.GetButtonDown("Fire2"))
        {
            Debug.Log(transform.rotation.y);
        }
    }

    public Vector3 GetMove()
    {
        return move;
    }

    void ApplyGravity()
    {
        RaycastHit hit;
        bool isHit = Physics.Raycast(transform.position, Vector3.down, out hit, 1.1f);

        if (isHit)
        {
            if (hit.transform.gameObject.tag == "Ground")
                player.isGrounded = true;
        }

        else
            player.isGrounded = false;

        if (!player.isGrounded)
        {
            var gravityForce = transform.up * player.gravity * Time.deltaTime;
            transform.Translate(gravityForce);
        }
    }
}
