using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMaster : MonoBehaviour
{
    [Header("Movement")]
    [System.NonSerialized] public float speed = 2f;
    [System.NonSerialized] public float gravity = -9.81f;
    [System.NonSerialized] public bool invertMovement = false;
    [System.NonSerialized] public bool freezeRotation = true;
    public bool isGrounded = false;
}
