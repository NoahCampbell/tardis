using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TARDISMaster : MonoBehaviour
{
    public Vector3 interiorTeleportPosition;
    public Vector3 exteriorTeleportPosition;
    InteriorTeleporter interiorScript;
    ExteriorTeleporter exteriorScript;
    GameObject interior;
    GameObject exterior;
    Transform objectToRotate;
    public bool dematerializing;
    bool goTransparent;
    public Material woodTransparentMaterial;
    public Material signTransparentMaterial;
    public Material policeSignTransparentMaterial;
    public Material lightTransparentMaterial;
    public Material handleTransparentMaterial;
    public Material woodMaterial;
    public Material signMaterial;
    public Material policeSignMaterial;
    public Material lightMaterial;
    public Material handleMaterial;
    public GameObject interiorViewMaterial;
    public GameObject exteriorViewMaterial;

    // Called before the first frame
    void Start()
    {
        interior = GameObject.Find("Interior");
        exterior = GameObject.Find("Exterior");
        objectToRotate = exterior.transform.Find("Rotateable");
        interiorScript = interior.GetComponent<InteriorTeleporter>();
        exteriorScript = objectToRotate.transform.Find("Openable Door").GetComponent<ExteriorTeleporter>();
        UpdateTeleportPositions();
        dematerializing = false;
        goTransparent = true;
    }

    public void OpenExteriorDoors()
    {
        exteriorScript.OpenDoors();
        OpenInteriorDoors();
    }

    public void OpenInteriorDoors()
    {
        interiorScript.OpenDoors();
    }

    public ConsoleMaster GetConsole()
    {
        return interiorScript.GetConsole();
    }

    public void RotateCrank()
    {
        interiorScript.RotateCrank();
    }

    public void TeleportPlayer(GameObject player)
    {
        exteriorScript.TeleportPlayer(player);
    }

    public void Teleport()
    {
        interiorScript.Teleport();
    }

    public void Increment()
    {
        interiorScript.Increment();
    }

    public void IncrementCoordinate(string buttonName)
    {
        interiorScript.IncrementCoordinate(buttonName);
    }

    public void UpdateTeleportPositions()
    {
        interiorTeleportPosition = interiorScript.GetTeleportPosition();

        exteriorTeleportPosition = exteriorScript.GetTeleportPosition();
    }

    public void SwitchButtonStatus()
    {
        interiorScript.SwitchButtonStatus();
    }

    public void RotateTo(Quaternion targetRotation)
    {
        objectToRotate.rotation = targetRotation;
    }

    public void Dematerialize()
    {
        if (!dematerializing)
        {
            var allChildren = exterior.transform.Cast<Transform>().ToArray();
            ApplyDematTextures(allChildren);
            StartCoroutine(DematAndRemat(null, "demat"));
        }
    }

    public void Rematerialize()
    {
        if (!dematerializing)
        {
            var allChildren = exterior.transform.Cast<Transform>().ToArray();
            StartCoroutine(DematAndRemat(allChildren, "remat"));
        }
    }

    public Quaternion GetRotation()
    {
        return objectToRotate.transform.rotation;
    }

    void ApplyDematTextures(Transform[] children)
    {
        foreach (Transform child in children)
        {
            if (child.childCount > 0)
            {
                var allChildren = child.transform.Cast<Transform>().ToArray();
                ApplyDematTextures(allChildren);
            }

            var renderer = child.GetComponent<MeshRenderer>();
            if (renderer != null)
            {
                Material[] newMaterials = renderer.materials;
                for (int i = 0; i < newMaterials.Length; i++)
                {
                    if (newMaterials[i].name.Contains("Wood"))
                    {
                        newMaterials[i] = woodTransparentMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Sign"))
                    {
                        newMaterials[i] = signTransparentMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Police"))
                    {
                        newMaterials[i] = policeSignTransparentMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Light"))
                    {
                        newMaterials[i] = lightTransparentMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Handle"))
                    {
                        newMaterials[i] = handleTransparentMaterial;
                    }
                }
                renderer.materials = newMaterials;
            }
        }
    }

    void ApplyRematTextures(Transform[] children)
    {
        foreach (Transform child in children)
        {
            if (child.childCount > 0)
            {
                var allChildren = child.transform.Cast<Transform>().ToArray();
                ApplyRematTextures(allChildren);
            }

            var renderer = child.GetComponent<MeshRenderer>();
            if (renderer != null)
            {
                Material[] newMaterials = renderer.materials;
                for (int i = 0; i < newMaterials.Length; i++)
                {
                    if (newMaterials[i].name.Contains("Wood"))
                    {
                        newMaterials[i] = woodMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Sign"))
                    {
                        newMaterials[i] = signMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Police"))
                    {
                        newMaterials[i] = policeSignMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Light"))
                    {
                        newMaterials[i] = lightMaterial;
                    }
                    else if (newMaterials[i].name.Contains("Handle"))
                    {
                        newMaterials[i] = handleMaterial;
                    }
                }
                renderer.materials = newMaterials;
            }
        }
    }

    IEnumerator DematAndRemat(Transform[] children, string status)
    {
        dematerializing = true;
        var timer = 0f;
        var shouldContinue = true;

        Material[] materials = new Material[] {woodTransparentMaterial, signTransparentMaterial, policeSignTransparentMaterial, 
                                               lightTransparentMaterial, handleTransparentMaterial};

        if (status == "demat")
            interiorViewMaterial.SetActive(false);

        while (shouldContinue)
        {
            foreach (Material material in materials)
            {
                string stringProperty = " ";
                if (material.HasProperty("_Color"))
                {
                    stringProperty = "_Color";
                }

                var newColor = material.GetColor(stringProperty);

                if (newColor.a <= 0.05f)
                {
                    goTransparent = false;
                }
                else if (newColor.a >= 0.65f)
                {
                    goTransparent = true;
                }

                if (goTransparent)
                {
                    newColor.a -= 0.008f;
                }
                else
                {
                    newColor.a += 0.008f;
                }

                if (timer >= 7f && newColor.a <= 0.05f && status == "demat")
                {
                    newColor.a = 0;
                    shouldContinue = false;
                }
                else if (timer >= 7f && newColor.a >= 0.65f && status == "remat")
                {
                    interiorViewMaterial.SetActive(true);
                    ApplyRematTextures(children);
                    shouldContinue = false;
                }

                material.SetColor(stringProperty, newColor);
            }
            timer += Time.deltaTime; 
            yield return new WaitForFixedUpdate();
        } 

        if (status == "remat")
        {
            foreach (Material material in materials)
            {
                var color = material.color;
                color.a = 1f;
                material.color = color;
            }
        }
        dematerializing = false;
    }
}