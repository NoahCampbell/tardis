using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExteriorTeleporter : MonoBehaviour
{
    GameObject exterior;
    bool doorOpen;
    Vector3 localDoorRotatePoint;
    bool doorOpening;
    bool doorClosing;
    TARDISMaster tardis;
    Vector3 closedPosition;
    Transform fixedDoor;
    Quaternion tardisRotation;

    // Start is called before the first frame update
    void Start()
    {
        exterior = GameObject.Find("Exterior");
        doorOpen = false;
        localDoorRotatePoint = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + 0.5f);
        doorOpening = false;
        doorClosing = false;
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
        closedPosition = transform.localPosition;
        fixedDoor = exterior.transform.GetChild(0).Find("Doors.002");
        tardisRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TeleportPlayer(GameObject player)
    {
        player.transform.position = tardis.interiorTeleportPosition;
        var playerRotation = player.transform.rotation;
        playerRotation = RotatePlayer(playerRotation);
        player.transform.rotation = playerRotation;
    }

    Quaternion RotatePlayer(Quaternion playerRotation)
    {
        tardisRotation = tardis.GetRotation();
        Vector3 eluerRotation = playerRotation.eulerAngles;
        switch(tardisRotation.y)
        {
            case -0.7071068f:
                eluerRotation = new Vector3(eluerRotation.x, eluerRotation.y + 90f, eluerRotation.z);
                break;
            case 0.7071068f:
                eluerRotation = new Vector3(eluerRotation.x, eluerRotation.y - 90f, eluerRotation.z);
                break;
            case 1f:
                eluerRotation = new Vector3(eluerRotation.x, eluerRotation.y + 180f, eluerRotation.z);
                break;
            default:
                return playerRotation;
        }
        
        playerRotation.eulerAngles = eluerRotation;
        return playerRotation;
    }

    public void OpenDoors()
    {
        if (doorOpen && !doorOpening)
        {
            doorOpen = false;
            StartCoroutine(CloseDoor());
        }
        else if (!doorOpen && !doorClosing)
        {
            doorOpen = true;
            StartCoroutine(OpenDoor());
        }
    }

    public Vector3 GetTeleportPosition()
    {
        Vector3 teleportPosition = exterior.transform.position;

        tardisRotation = tardis.GetRotation();
        switch(tardisRotation.y)
        {
            case -0.7071068f:
                teleportPosition.x -= 1.5f;
                teleportPosition.y += 1.15f;
                teleportPosition.z += 1.2f;
                break;
            case 0.7071068f:
                teleportPosition.x -= 1.4f;
                teleportPosition.y += 1.15f;
                teleportPosition.z += 2.8f;
                break;
            case 1f:
                teleportPosition.x -= 0.7f;
                teleportPosition.y += 1.15f;
                teleportPosition.z += 1.9f;
                break;
            default:
                teleportPosition.x -= 2.2f;
                teleportPosition.y += 1.15f;
                teleportPosition.z += 1.9f;
                break;
        }
        return teleportPosition;
    }

    IEnumerator OpenDoor()
    {
        doorOpening = true;
        float timer = 1f;
        float speed = 79f;
        Vector3 pivot = exterior.transform.GetChild(0).TransformPoint(localDoorRotatePoint);
        while (timer > 0)
        {
            transform.RotateAround(pivot, Vector3.up, speed * Time.deltaTime);
            yield return new WaitForFixedUpdate();
            timer -= Time.deltaTime;
        }

        doorOpening = false;
    }

    IEnumerator CloseDoor()
    {
        doorClosing = true;
        float timer = 1f;
        float speed = 79f;
        Vector3 pivot = exterior.transform.GetChild(0).TransformPoint(localDoorRotatePoint);

        while (timer > 0)
        {
            transform.RotateAround(pivot, Vector3.down, speed * Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
            timer -= Time.deltaTime;
        }
        transform.rotation = fixedDoor.rotation;
        transform.localPosition = closedPosition;
        doorClosing = false;
    }
}
