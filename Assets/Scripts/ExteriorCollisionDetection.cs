using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExteriorCollisionDetection : MonoBehaviour
{
    TARDISMaster tardis;

    // Start is called before the first frame update
    void Start()
    {
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player"))
        {
            tardis.TeleportPlayer(other.gameObject);
        }
    }
}
