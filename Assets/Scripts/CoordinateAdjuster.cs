using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoordinateAdjuster : MonoBehaviour
{
    TARDISMaster tardis;
    ConsoleMaster console;
    CoordinateIncrementor incrementor;
    GameObject xButton;
    GameObject yButton;
    GameObject zButton;
    Vector3 targetCoords;
    CoordinateDisplay display;
    CoordinateStatusButton statusButton;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
        yield return new WaitForSeconds(0.1f);
        console = tardis.GetConsole();
        incrementor = console.GetIncrementor();
        targetCoords = new Vector3(0f, 0.5f, 0f);
        display = console.GetDisplay();
        statusButton = console.GetStatusButton();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncrementCoordinate(string buttonName)
    {
        if (buttonName == "X Button" && Mathf.Abs(targetCoords.x + (incrementor.GetStatus() * statusButton.GetButtonStatus())) <= 10000)
        {
            targetCoords.x += (incrementor.GetStatus() * statusButton.GetButtonStatus());
        }
        else if (buttonName == "Y Button" && Mathf.Abs(targetCoords.y + (incrementor.GetStatus() * statusButton.GetButtonStatus())) <= 10000)
        {
            targetCoords.y += (incrementor.GetStatus() * statusButton.GetButtonStatus());
        }
        else if (buttonName == "Z Button" && Mathf.Abs(targetCoords.z + (incrementor.GetStatus() * statusButton.GetButtonStatus())) <= 10000)
        {
            targetCoords.z += (incrementor.GetStatus() * statusButton.GetButtonStatus());
        }

        display.UpdateDisplayCoords(targetCoords);
    }

    public Vector3 GetTargetCoords()
    {
        return targetCoords;
    }
}
