using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPartsController : MonoBehaviour
{
    bool wentThroughTARDIS;
    bool invertedMovement;
    PlayerMovement playerMovement;

    // Start is called before the first frame update
    void Start()
    {
        playerMovement = GameObject.Find("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (invertedMovement)
        {
            Move();
        }
    }

    void Move()
    {
        Vector3 move = playerMovement.GetMove();

        if (move == Vector3.zero)
        {
            return;
        }
        move = move.normalized * Time.deltaTime;
        transform.Translate(-move, Space.World);

    }

    public void InvertMovement(bool status)
    {
        invertedMovement = status;
    }

    public void SetTARDISStatus(bool status)
    {
        wentThroughTARDIS = status;
    }

    public bool GetTARDISSatus()
    {
        return wentThroughTARDIS;
    }

    public bool GetInvertMovement()
    {
        return invertedMovement;
    }
}
