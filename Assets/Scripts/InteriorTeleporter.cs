using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteriorTeleporter : MonoBehaviour
{
    GameObject interior;
    TARDISMaster tardis;
    ConsoleMaster console;
    InteriorDoorScript door;

    // Start is called before the first frame update
    void Awake()
    {
        interior = GameObject.Find("Interior");
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
        console = GameObject.Find("Console").GetComponent<ConsoleMaster>();
        door = transform.Find("Interior Doors").GetComponent<InteriorDoorScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Teleport()
    {
        console.Teleport();
    }

    public void Increment()
    {
        console.Increment();
    }

    public void IncrementCoordinate(string buttonName)
    {
        console.IncrementCoordinate(buttonName);
    }

    public Vector3 GetTeleportPosition()
    {
        var teleportPosition = door.transform.position;
        teleportPosition.x += 2.5f;
        teleportPosition.y += 1.25f;
        teleportPosition.z -= 2.35f;
        return teleportPosition;
    }

    public void SwitchButtonStatus()
    {
        console.SwitchButtonStatus();
    }

    public void RotateCrank()
    {
        console.RotateCrank();
    }

    public void OpenDoors()
    {
        door.OpenDoors();
    }

    public ConsoleMaster GetConsole()
    {
        return console;
    }
}
