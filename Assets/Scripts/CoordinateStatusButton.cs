using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoordinateStatusButton : MonoBehaviour
{
    float status;
    GameObject button;
    Vector3 decrementPosition;
    Vector3 incrementPosition;

    // Start is called before the first frame update
    void Start()
    {
        status = 1;
        button = transform.Find("Button").gameObject;
        decrementPosition = new Vector3(-0.05f, 0.008f, -0.07f);
        incrementPosition = new Vector3(0.05f, 0.008f, -0.07f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchStatus()
    {
        status *= -1;

        if (status < 0)
        {
            button.transform.localPosition = decrementPosition;
        }
        else
        {
            button.transform.localPosition = incrementPosition;
        }
    }

    public float GetButtonStatus()
    {
        return status;
    }
}
