using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoordinateDisplay : MonoBehaviour
{
    TextMeshPro xDisplay;
    TextMeshPro yDisplay;
    TextMeshPro zDisplay;

    // Start is called before the first frame update
    void Start()
    {
        xDisplay = transform.Find("X Coords").GetComponent<TextMeshPro>();
        yDisplay = transform.Find("Y Coords").GetComponent<TextMeshPro>();
        zDisplay = transform.Find("Z Coords").GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateDisplayCoords(Vector3 newCoords)
    {
        xDisplay.text = "X: " + newCoords.x;
        yDisplay.text = "Y: " + (newCoords.y - 0.5f);
        zDisplay.text = "Z: " + newCoords.z;
    }
}
