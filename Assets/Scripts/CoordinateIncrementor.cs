using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoordinateIncrementor : MonoBehaviour
{
    float status;
    GameObject button;

    // Start is called before the first frame update
    void Start()
    {
        status = 1;
        button = transform.Find("Incrementor Button").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Increment()
    {
        if (status == 1)
            status = 10;
        else if (status == 10)
            status = 100;
        else
            status = 1;

        MoveIncrementor();
    }

    void MoveIncrementor()
    {
        if (status != 1)
            button.transform.Translate(0.1f, 0f, 0f);
        else
            button.transform.Translate(-0.2f, 0f, 0f);
        
    }

    public float GetStatus()
    {
        return status;
    }
}
