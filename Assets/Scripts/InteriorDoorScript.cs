using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteriorDoorScript : MonoBehaviour
{
    bool doorOpen;
    GameObject door;
    Vector3 localDoorRotatePoint;
    bool doorOpening;
    bool doorClosing;
    TARDISMaster tardis;
    Vector3 closedPosition;
    Vector3 closedRotation;
    Quaternion tardisRotation;

    // Start is called before the first frame update
    void Start()
    {
        doorOpen = false;
        door = transform.Find("Openable Door").gameObject;
        localDoorRotatePoint = new Vector3(door.transform.localPosition.x, door.transform.localPosition.y, door.transform.localPosition.z + 0.5f);
        doorOpening = false;
        doorClosing = false;
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
        closedPosition = new Vector3(-1.93200004f, 0.889999866f, 2.48399997f);
        closedRotation = new Vector3(-90f, 0f, 0f);
        tardisRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDoors()
    {
        if (doorOpen && !doorOpening)
        {
            doorOpen = false;
            StartCoroutine(CloseDoor());
        }
        else if (!doorOpen && !doorClosing)
        {
            doorOpen = true;
            StartCoroutine(OpenDoor());
        }
    }

    // If the player collides with this object when the door is open, 
    // teleports the player and rotates them to face the new "forward"
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player") && doorOpen)
        {
            other.transform.position = tardis.exteriorTeleportPosition;

            var playerRotation = other.gameObject.transform.rotation;
            playerRotation = RotatePlayer(playerRotation);
            other.gameObject.transform.rotation = playerRotation;
        }
    }
    
    Quaternion RotatePlayer(Quaternion playerRotation)
    {
        tardisRotation = tardis.GetRotation();   
        Vector3 eluerRotation = playerRotation.eulerAngles;
        switch(tardisRotation.y)
        {
            case 0.7071068f:
                eluerRotation = new Vector3(eluerRotation.x, eluerRotation.y + 90f, eluerRotation.z);
                break;
            case -0.7071068f:
                eluerRotation = new Vector3(eluerRotation.x, eluerRotation.y - 90f, eluerRotation.z);
                break;
            case 1:
                eluerRotation = new Vector3(eluerRotation.x, eluerRotation.y + 180f, eluerRotation.z);
                break;
            default:
                return playerRotation;
        }
        
        playerRotation.eulerAngles = eluerRotation;
        return playerRotation;
    }

    IEnumerator OpenDoor()
    {
        doorOpening = true;
        float timer = 1f;
        float speed = 80f;
        Vector3 pivot = transform.TransformPoint(localDoorRotatePoint);
        while (timer > 0)
        {
            door.transform.RotateAround(pivot, Vector3.up, speed * Time.deltaTime);
            yield return new WaitForFixedUpdate();
            timer -= Time.deltaTime;
        }

        doorOpening = false;
    }

    IEnumerator CloseDoor()
    {
        doorClosing = true;
        float timer = 1f;
        float speed = 75f;
        Vector3 pivot = transform.TransformPoint(localDoorRotatePoint);

        while (timer > 0)
        {
            door.transform.RotateAround(pivot, Vector3.down, speed * Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
            timer -= Time.deltaTime;
        }
        
        var quatRotation = Quaternion.identity;
        quatRotation.eulerAngles = closedRotation;
        door.transform.rotation = quatRotation;
        
        door.transform.localPosition = closedPosition;

        doorClosing = false;
    }
}
