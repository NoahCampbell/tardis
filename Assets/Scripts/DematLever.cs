using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DematLever : MonoBehaviour
{
    bool isOn;
    GameObject target;
    TARDISMaster tardis;
    ConsoleMaster console;
    CoordinateAdjuster adjuster;
    TARDISRotation rotator;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        target = GameObject.Find("Exterior");
        tardis = GameObject.Find("TARDIS Master").GetComponent<TARDISMaster>();
        isOn = false;
        yield return new WaitForSeconds(0.5f);
        console = tardis.GetConsole();
        adjuster = console.GetAdjuster();
        rotator = console.GetRotator();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Teleport()
    {
        target.transform.position = adjuster.GetTargetCoords();
        tardis.RotateTo(rotator.GetTargetRotation());
        tardis.UpdateTeleportPositions();
    }

    public void SetStatus()
    {
        if (!tardis.dematerializing)
        {
            isOn = !isOn;

            if (isOn)
            {
                tardis.Dematerialize();
                StartCoroutine(FlickLeverOn());
            }
            else
            {
                Teleport();
                tardis.Rematerialize();
                StartCoroutine(FlickLeverOff());
            }
        }
    }
    
    IEnumerator FlickLeverOn()
    {
        while (transform.rotation.eulerAngles.z != 90f)
        {
            transform.Rotate(0, 0, 10);
            yield return new WaitForSeconds(0.03f);
        }
    }

    IEnumerator FlickLeverOff()
    {
        while (transform.rotation.eulerAngles.z > 10f)
        {
            transform.Rotate(0, 0, -10);
            yield return new WaitForSeconds(0.03f);
        }
    }
}
