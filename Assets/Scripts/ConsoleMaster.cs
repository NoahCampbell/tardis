using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleMaster : MonoBehaviour
{
    DematLever demat;
    CoordinateIncrementor incrementor;
    CoordinateAdjuster adjuster;
    CoordinateDisplay display;
    CoordinateStatusButton statusButton;
    TARDISRotation rotator;

    // Start is called before the first frame update
    void Awake()
    {
        incrementor = transform.Find("Coordinate Incrementor").GetComponent<CoordinateIncrementor>();
        adjuster = transform.Find("Coordinate Adjusters").GetComponent<CoordinateAdjuster>();
        demat = GameObject.Find("Lever").GetComponent<DematLever>();
        display = GameObject.Find("Display").GetComponent<CoordinateDisplay>();
        statusButton = transform.Find("Coordinate Status").GetComponent<CoordinateStatusButton>();
        rotator = transform.Find("Rotation Wheel").GetComponent<TARDISRotation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Teleport()
    {
        demat.SetStatus();
    }

    public void Increment()
    {
        incrementor.Increment();
    }

    public void IncrementCoordinate(string buttonName)
    {
        adjuster.IncrementCoordinate(buttonName);
    }

    public void RotateCrank()
    {
        rotator.RotateCrank();
    }

    public void SwitchButtonStatus()
    {
        statusButton.SwitchStatus();
    }

    public CoordinateIncrementor GetIncrementor()
    {
        return incrementor;
    }

    public CoordinateAdjuster GetAdjuster()
    {
        return adjuster;
    }

    public CoordinateDisplay GetDisplay()
    {
        return display;
    }

    public CoordinateStatusButton GetStatusButton()
    {
        return statusButton;
    }

    public TARDISRotation GetRotator()
    {
        return rotator;
    }

}
